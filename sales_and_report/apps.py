from django.apps import AppConfig


class SalesAndReportConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sales_and_report'
