# syntax=docker/dockerfile:1.4

FROM python:3.10
EXPOSE 8000
WORKDIR /app
COPY . /app
RUN pip3 install -r requirements.txt --no-cache-dir

MD ["python", "manage.py", "makemigrations"]
MD ["python", "manage.py", "migrate"]
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
